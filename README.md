**This is the companion repository for the Bachelor Thesis "Runtime Reconfiguration of Filter Operators in Stream Processing Systems".**

This repository contains the following:

- Apache Flink version 1.18.0
- Implementation for our new Filter Operator
- several tests to evaluate the new Operator


## Apache Flink version 1.18.0
This implementation is found in the `flink-1.18.0` folder.

## Filter Operator
A working implementation of our Filter operator can be found in the `reconfiguration` folder. The operator is implemented in the `CompilingFilterFunction.java` file in `reconfiguration/src/main/java/reconf/`.

You can run this job by navigating to this directory: `flink-1.18.0/`.
From there, execute the following commands:
`./bin/start-cluster.sh`
`./bin/flink run ../reconfiguration/target/reconfiguration-0.1.jar`

To change the behavior of the Filter operator you can modify the configuration file located in `flink-1.18.0/filters/Filter2.java`

You can observed the results of this query either by the generated output logs in `flink-1.18.0/log` or in the Flink Web Dashboard under `http://localhost:8081/` while a cluster is active.


A similar operator that cannot be reconfigured at runtime can be found in `standard/src/main/java/reconf/` under `StandardFilterOperator.java`.

## Automated tests
All test included in the thesis are available in this repository

- Time-To-Apply Test (for our new Filter operator and the standard Flink operator)
- Throughput Test with no reconfiguration (for our new Filter operator and the standard Flink operator)
- Throughput Test with reconfiguration (for our new Filter operator and the standard Flink operator)

### How to run the automated tests
Before attempting to run the test ensure that you have:
- Unix System (e.g. Linux)
- Java 11
- Python 3.x

Navigate to `flink-1.18.0/`. You must always be in this directory when running tests!

- Time-To-Apply-Test
    - New Filter operator: Run `python3 ./tests/timetoapply_reconf.py`
    - Standard Flink operatpor: Run `python3 ./tests/timetoapply_standard.py`
- Throughput Test with no reconfiguration
    - New Filter operator: Run `python3 ./tests/throughput_reconf.py`
    - Standard Flink operator: Run `python3 ./tests/throughput_standard.py`
- Throughput Test with reconfiguration
    - New Filter operator: Run `python3 ./tests/througput_reconf2.py`
    - Standard Flink operator: Run `python3 ./tests/througput_standard2.py`

The results for these tests will be stored in `flink-1.18.0/tests/results`.

If you want to run both Time-To-Apply tests and both Throughput with no reconfiguration tests multiple times, you may also run: `python3 ./tests/run_all_tests.py`. This will run each of those test 10 times (number can be altered by modifying the python file).

You can also generate the plots as depicted in the thesis:
- Throughput with no reconfiguration comparison: Run `python3 ./tests/generate-throughput-results.py` after running both throughput with no reconfiguration tests at least once (more iterations are better though).
- Throughput with reconfiguration line plot: Run `python3 ./tests/througput_reconf2.py` and `python3 ./tests/througput_standard2.py`.
- The Time-To-Apply plot cannot be automatically generated.
