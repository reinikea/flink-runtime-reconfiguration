# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23

Author: Alexander Reinicke

Runtime Reconfiguration of Filter Operators in Stream Processing Systems

Throughput Test Standard 2 (with reconfiguration)

"""

import subprocess
from subprocess import PIPE
import time
import os
import glob
import matplotlib.pyplot as plt
import datetime
import numpy as np

def process_file(file_path):
    # Initialize variables
    data_points = {}
    start_time = None
    end_time = None

    vert_line1 = 0
    vert_line2 = 0
    vert_line3 = 0

    # Process each line in the file
    with open(file_path, 'r') as file:
        for line in file:

            if line[0:2] != "20":
                if line.split(' ',1)[0] == "Time_1:":
                    vert_line1 = line.split(' ',1)[1][:-1]
                    vert_line1 = datetime.datetime.strptime(vert_line1, "%Y-%m-%d %H:%M:%S.%f")
                    if start_time is not None:
                        vert_line1 = (vert_line1 - start_time).total_seconds()
                elif line.split(' ',1)[0] == "Time_2:":
                    vert_line2 = line.split(' ',1)[1][:-1]
                    vert_line2 = datetime.datetime.strptime(vert_line2, "%Y-%m-%d %H:%M:%S.%f")
                    if start_time is not None:
                        vert_line2 = (vert_line2 - start_time).total_seconds()
                elif line.split(' ',1)[0] == "Time_3:":
                    vert_line3 = line.split(' ',1)[1][:-1]
                    vert_line3 = datetime.datetime.strptime(vert_line3, "%Y-%m-%d %H:%M:%S.%f")
                    if start_time is not None:
                        vert_line3 = (vert_line3 - start_time).total_seconds()
                continue

            timestamp_str, _ = line.strip().split('---')
            timestamp = datetime.datetime.strptime(timestamp_str, "%Y-%m-%d %H:%M:%S.%f")

            # Set start and end times
            if start_time is None or timestamp < start_time:
                start_time = timestamp
            if end_time is None or timestamp > end_time:
                end_time = timestamp

            # Calculate time in seconds from the start
            time_diff = (timestamp - start_time).total_seconds()

            # Rounding down to the nearest 0.001s (100th of a second)
            interval = round(time_diff, 3)
            
            # Count the elements
            data_points[interval] = data_points.get(interval, 0) + 1

    # Create a full range of time intervals from start to end
    total_duration = (end_time - start_time).total_seconds()
    all_intervals = np.arange(0, total_duration, 0.001)
    all_intervals = np.round(all_intervals, 3)

    # Prepare lists for plotting, including intervals with zero elements
    times = list(all_intervals)
    #counts = [data_points.get(time, 0) * 1000 for time in times]  # Multiply by 100
    counts = [data_points.get(time, 0) for time in times] 

    return times, counts, vert_line1, vert_line2, vert_line3

def plot_data(times, counts, vert_line1, vert_line2, vert_line3):
    plt.plot(times, counts, label="Processed elements")
    plt.xlabel('Time (seconds from start)')
    plt.ylabel('Number of Elements Processed per 0.001s')
    plt.title('Elements Processed Over Time')
    #plt.yscale('log')

    plt.xlim([vert_line1 - 0.3, vert_line3 + 2.5])

    plt.axvline(x=vert_line1, color='r', linestyle='--', linewidth=1, label="Cancellation submitted")
    plt.axvline(x=vert_line2, color='b', linestyle='--', linewidth=1, label="Query has stopped")
    plt.axvline(x=vert_line3, color='g', linestyle='--', linewidth=1, label="Query has resumed")
    plt.legend(loc="lower right")

    # Save the plot to a file
    plt.savefig('./tests/results/processed_elements_standard.png', format='png')
    
    plt.show()



sleep_time = 3

# Start Cluster
subprocess.run('./bin/start-cluster.sh')

# Start Query and wait until the command returns
result = subprocess.run('./bin/flink run --detached ../tests/test_standard_count2/standard/target/standard-0.1.jar', stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
job_id = result[result.find("JobID")+6:-1]

time.sleep(sleep_time)

# Start Query and wait until the command returns
time_1 = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
result = subprocess.run('./bin/flink stop --type native --savepointPath /tmp/flink-savepoints ' + job_id, stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
savepoint_id = result[result.find("flink-savepoints")+17:-1]
time_2 = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
result = subprocess.run('./bin/flink run --detached --fromSavepoint /tmp/flink-savepoints/' + savepoint_id + ' ../tests/test_standard_count2/standard/target/standard-0.1.jar', stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
time_3 = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
job_id = result[result.find("JobID")+6:-1]
    
# Wait before ending the query
time.sleep(sleep_time)

subprocess.run("./bin/flink cancel " + job_id, shell=True)
subprocess.run("./bin/stop-cluster.sh")

# Get file path
ident = "taskexecutor"
basepath = "./log/"

filePaths = glob.glob(os.path.join(basepath, '*{0}*.out'.format(ident)))
file_path = filePaths[0]

file = open(file_path,"a")
file.write("Time_1: " + time_1 + "\n")
file.write("Time_2: " + time_2 + "\n")
file.write("Time_3: " + time_3 + "\n")
file.close()

# Replace 'your_data_file.txt' with your file's path
times, counts, vert_line1, vert_line2, vert_line3 = process_file(file_path)
plot_data(times, counts, vert_line1, vert_line2, vert_line3)



# Remove previous log files
subprocess.run('rm ./log/*', shell=True)
