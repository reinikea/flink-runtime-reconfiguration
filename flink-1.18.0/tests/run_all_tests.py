# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23

Author: Alexander Reinicke

Runtime Reconfiguration of Filter Operators in Stream Processing Systems

Runs all Tests

"""
import subprocess
import sys

arglist = []
if len(sys.argv) < 1:
    arglist = sys.argv
else :
    arglist = [1, 2, 3, 4]

iterations = 10

for i in range(iterations):

    if 1 in arglist:
        subprocess.run('python3 ./tests/timetoapply_reconf.py', shell = True)
        print(f">>>>> Finished Running Time-To-Apply-Test Reconf - Iteration {i+1}")

    if 2 in arglist:
        subprocess.run('python3 ./tests/timetoapply_standard.py', shell = True)
        print(f">>>>> Finished Running Time-To-Apply-Test Standard - Iteration {i+1}")

    if 3 in arglist:
        subprocess.run('python3 ./tests/throughput_reconf.py', shell = True)
        print(f">>>>> Finished Running Throughput-Test Reconf - Iteration {i+1}")

    if 4 in arglist:
        subprocess.run('python3 ./tests/throughput_standard.py', shell = True)
        print(f">>>>> Finished Running Throughput-Test Standard - Iteration {i+1}")


print("-----------------------------------")
print(">>>>> Finished Running All Tests")
