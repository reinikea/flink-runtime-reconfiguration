# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23

Author: Alexander Reinicke

Runtime Reconfiguration of Filter Operators in Stream Processing Systems

Time To Apply Test Reconfiguration

"""

import subprocess
from subprocess import PIPE
import time
from datetime import datetime
import random
import os
import glob

# Setup time lists
time_file_changed = []
time_file_change_detected = []
time_new_udf_received = []
time_new_udf_compiled = []

# Set iteration amount and sleep time between changes
iterations = 5
sleep_time = 5

# Base Code
udf_code1 = 'static {System.out.println("Class initially loaded!"); }public boolean filter(Long value) {return value >= '
random_int = 10
udf_code2 = ";}"

# Start Cluster
subprocess.run('./bin/start-cluster.sh')

# Start Query and wait until the command returns
result = subprocess.run('./bin/flink run --detached ../tests/test_timetoapply_reconf/reconfiguration/target/reconfiguration-0.1.jar', stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
job_id = result[result.find("JobID")+6:-1]

time.sleep(sleep_time)

# Loop makes changes to config file
for i in range(iterations):
    # Change File
    # Determine new random int
    random_int = random.randint(0, 20)
    
    # Replace String in File
    file = open("./filters/Filter2.java","w")
    newUDF = udf_code1 + str(random_int) + udf_code2
    file.write(newUDF)
    file.close()
    
    # Save Current Time
    time_file_changed.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
    
    # Wait before making the next change
    time.sleep(sleep_time)


# End Query
subprocess.run("./bin/flink cancel " + job_id, shell = True)
   
# Loop gathers Flink Output
ident = "taskexecutor"
basepath = "./log/"

filePaths = glob.glob(os.path.join(basepath, '*{0}*.out'.format(ident)))
filename = filePaths[0]


file = open(filename,"r")
lines = file.readlines()
file.close()

for line in lines:
    command = line.split(' ', 1)[0]
    date = line.split(' ', 1)[1]
    
    if command == "FM_Change_Detected:":
        time_file_change_detected.append(date[:-1])
    
    elif command == "FO_UDF_Received:":
        time_new_udf_received.append(date[:-1])
        
    elif command == "FO_UDF_Compiled:":
        time_new_udf_compiled.append(date[:-1])
        
    else:
        continue
        
# Make certain assertions to check if results are valid
assert len(time_file_changed) == iterations, "Unexpected amount of elements in list: " + str(len(time_file_changed))
assert len(time_file_change_detected) == iterations + 1, "Unexpected amount of elements in list: " + str(len(time_file_change_detected))
assert len(time_new_udf_received) == iterations + 1, "Unexpected amount of elements in list: " + str(len(time_new_udf_received))
assert len(time_new_udf_compiled) == iterations + 1, "Unexpected amount of elements in list: " + str(len(time_new_udf_compiled))
        

# Write Collected Dates to Output File by appending
# Ignore 1st entry in all but the first list because these entries are created when the job
# is initially started and are thus not part of this test

file = open("./tests/results/timetoapply_reconf-results.txt","a")
for i in range(iterations):
    file.write("1: " + time_file_changed[i] + "\n")
    file.write("2: " + time_file_change_detected[i+1] + "\n")
    file.write("3: " + time_new_udf_received[i+1] + "\n")
    file.write("4: " + time_new_udf_compiled[i+1] + "\n")
    file.write("-----------------------------\n")
file.close()


# Stop Flink Cluster
time.sleep(sleep_time)
# Stop Cluster
subprocess.run('./bin/stop-cluster.sh')

# Remove previous log files
subprocess.run('rm ./log/*', shell=True)
