import matplotlib.pyplot as plt
import numpy as np


def read_data(file_path):
    with open(file_path, 'r') as file:
        return np.array([float(line.strip()) / 10 for line in file])

array1 = read_data('./tests/results/throughput_reconf-results.txt')
array2 = read_data('./tests/results/throughput_standard-results.txt')

mean1, std1 = np.median(array1), np.std(array1)
mean2, std2 = np.median(array2), np.std(array2)


labels = ['New Filter operator', 'Standard Flink operator']
means = [mean1, mean2]
errors = [std1, std2]

bars = plt.bar(labels, means, yerr=errors, capsize=10, color=['blue', 'orange'])
plt.xlabel("Operator Type")
plt.ylabel("Number of processed elements per second")
plt.title("Comparison of Throughput by Operator Type")

for bar, mean in zip(bars, means):
    y_val = bar.get_height()
    plt.text(bar.get_x() + bar.get_width() / 3 * 2.6, y_val, round(mean), ha='center', va='bottom')

plt.show()

# Save the plot to a file
plt.savefig('./tests/results/throughput_comparison.png', format='png', bbox_inches="tight")
