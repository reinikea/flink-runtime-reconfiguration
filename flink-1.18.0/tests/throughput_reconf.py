# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23

Author: Alexander Reinicke

Runtime Reconfiguration of Filter Operators in Stream Processing Systems

Throughput Test Reconfiguration

"""
import subprocess
from subprocess import PIPE
import time
import os
import glob

# Base Code
udf_code1 = 'static {System.out.println("Class initially loaded!"); }public boolean filter(Long value) {return Integer.parseInt(value.toString()) >= '
random_int = 0
udf_code2 = ";}"

# Set iteration amount and sleep time between changes
iterations = 5
sleep_time = 10

number_of_items = []

# Set Filter
file = open("./filters/Filter2.java","w")
newUDF = udf_code1 + str(random_int) + udf_code2
file.write(newUDF)
file.close()


for i in range(iterations):

    # Start Cluster
    subprocess.run('./bin/start-cluster.sh')
    time.sleep(3)

    # Start Query and wait until the command returns
    result = subprocess.run('./bin/flink run --detached ../tests/test_reconfiguration_count/reconfiguration/target/reconfiguration-0.1.jar', stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
    job_id = result[result.find("JobID")+6:-1]

    time.sleep(sleep_time)

    # Stop Cluster
    subprocess.run('./bin/flink cancel ' + job_id, shell=True)
    # Stop Cluster
    subprocess.run('./bin/stop-cluster.sh')

    time.sleep(1)

    # Count number of items in log file
    ident = "taskexecutor"
    basepath = "./log/"

    filePaths = glob.glob(os.path.join(basepath, '*{0}*.out'.format(ident)))
    filename = filePaths[0]


    file = open(filename,"r")
    lines = file.readlines()
    file.close()

    number_of_items.append(len(lines))

    # Remove previous log files
    subprocess.run('rm ./log/*', shell=True)

    time.sleep(1)



file = open("./tests/results/throughput_reconf-results.txt","a")
for i in range(iterations):
    file.write(str(number_of_items[i]) + "\n")
file.close()


