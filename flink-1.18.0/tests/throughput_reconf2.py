# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23

Author: Alexander Reinicke

Runtime Reconfiguration of Filter Operators in Stream Processing Systems

Throughput Test Reconfiguration 2 (with reconfiguration)

"""
import subprocess
from subprocess import PIPE
import time
import os
import glob
import matplotlib.pyplot as plt
import datetime
import numpy as np


def process_file(file_path):
    # Initialize variables
    data_points = {}
    start_time = None
    end_time = None

    vert_line1 = 0
    vert_line2 = 0
    vert_line3 = 0
    vert_line4 = 0

    # Process each line in the file
    with open(file_path, 'r') as file:
        for line in file:

            if line[0:2] != "20":
                if line.split(' ',1)[0] == "FO_UDF_Received:":
                    vert_line1 = line.split(' ',1)[1][:-1]
                    vert_line1 = datetime.datetime.strptime(vert_line1, "%Y-%m-%d %H:%M:%S.%f")
                    if start_time is not None:
                        vert_line1 = (vert_line1 - start_time).total_seconds()
                elif line.split(' ',1)[0] == "FO_UDF_Compiled:":
                    vert_line2 = line.split(' ',1)[1][:-1]
                    vert_line2 = datetime.datetime.strptime(vert_line2, "%Y-%m-%d %H:%M:%S.%f")
                    if start_time is not None:
                        vert_line2 = (vert_line2 - start_time).total_seconds()
                elif line.split(' ',1)[0] == "FM_Change_Detected:":
                    vert_line3 = line.split(' ',1)[1][:-1]
                    vert_line3 = datetime.datetime.strptime(vert_line3, "%Y-%m-%d %H:%M:%S.%f")
                    if start_time is not None:
                        vert_line3 = (vert_line3 - start_time).total_seconds()
                elif line.split(' ',1)[0] == "PP_Changes_Written:":
                    vert_line4 = line.split(' ',1)[1][:-1]
                    vert_line4 = datetime.datetime.strptime(vert_line4, "%Y-%m-%d %H:%M:%S.%f")
                    if start_time is not None:
                        vert_line4 = (vert_line4 - start_time).total_seconds()

                continue

            timestamp_str, _ = line.strip().split('---')
            timestamp = datetime.datetime.strptime(timestamp_str, "%Y-%m-%d %H:%M:%S.%f")

            # Set start and end times
            if start_time is None or timestamp < start_time:
                start_time = timestamp
            if end_time is None or timestamp > end_time:
                end_time = timestamp

            # Calculate time in seconds from the start
            time_diff = (timestamp - start_time).total_seconds()

            # Rounding down to the nearest 0.01s (100th of a second)
            interval = round(time_diff, 3)
            
            # Count the elements
            data_points[interval] = data_points.get(interval, 0) + 1

    # Create a full range of time intervals from start to end
    total_duration = (end_time - start_time).total_seconds()
    all_intervals = np.arange(0, total_duration, 0.001)
    all_intervals = np.round(all_intervals, 3)

    # Prepare lists for plotting, including intervals with zero elements
    times = list(all_intervals)
    #counts = [data_points.get(time, 0) * 1000 for time in times]  # Multiply by 1000
    counts = [data_points.get(time, 0) for time in times] 

    return times, counts, vert_line1, vert_line2, vert_line3, vert_line4

def plot_data(times, counts, vert_line1, vert_line2, vert_line3, vert_line4):
    plt.plot(times, counts, label="Processed elements")
    plt.xlabel('Time (seconds from start)')
    plt.ylabel('Number of Elements Processed per 0.001s')
    plt.title('Elements Processed Over Time')
    #plt.yscale('log')

    #plt.xlim([0.5, max(times)])
    plt.xlim([vert_line4 - 0.05, vert_line2 + 0.25])

    plt.axvline(x=vert_line4, color='y', linestyle='--', linewidth=1, label="Changes written")
    plt.axvline(x=vert_line3, color='g', linestyle='--', linewidth=1, label="Changes read")
    plt.axvline(x=vert_line1, color='r', linestyle='--', linewidth=1, label="Changes received")
    plt.axvline(x=vert_line2, color='b', linestyle='--', linewidth=1, label="Changes compiled")
    plt.legend(loc="lower right")
    #plt.legend(bbox_to_anchor=(0, -0.1))

    # Save the plot to a file
    plt.savefig('./tests/results/processed_elements_reconf.png', format='png')
    
    plt.show()



# Base Code
udf_code_1 = 'static {System.out.println("Class initially loaded!"); }public boolean filter(Long value) {return value >= -1;}'
udf_code_2 = 'static {System.out.println("Class initially loaded!"); }public boolean filter(Long value) {return value >= 0;}'
sleep_time = 3

# Replace String in File
file = open("./filters/Filter2.java","w")
file.write(udf_code_1)
file.close()

# Start Cluster
subprocess.run('./bin/start-cluster.sh')

# Start Query and wait until the command returns
result = subprocess.run('./bin/flink run --detached ../tests/test_reconfiguration_count2/reconfiguration/target/reconfiguration-0.1.jar', stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
job_id = result[result.find("JobID")+6:-1]

time.sleep(sleep_time)

# Replace String in File
file = open("./filters/Filter2.java","w")
time_changed = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
file.write(udf_code_2)
file.close()

time.sleep(sleep_time)

# End Query
subprocess.run("./bin/flink cancel " + job_id, shell = True)
subprocess.run("./bin/stop-cluster.sh")

# Get file path
ident = "taskexecutor"
basepath = "./log/"

filePaths = glob.glob(os.path.join(basepath, '*{0}*.out'.format(ident)))
file_path = filePaths[0]

file = open(file_path,"a")
file.write("PP_Changes_Written: " + time_changed)
file.close()

# Replace 'your_data_file.txt' with your file's path
times, counts, vert_line1, vert_line2, vert_line3, vert_line4 = process_file(file_path)
plot_data(times, counts, vert_line1, vert_line2, vert_line3, vert_line4)



# Remove previous log files
subprocess.run('rm ./log/*', shell=True)

