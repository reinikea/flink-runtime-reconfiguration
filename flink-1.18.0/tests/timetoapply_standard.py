# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23

Author: Alexander Reinicke

Runtime Reconfiguration of Filter Operators in Stream Processing Systems

Time To Apply Test Standard

"""

import subprocess
from subprocess import PIPE
import time
from datetime import datetime
import random

# Setup time lists
time_pre_savepoint = []
time_post_savepoint = []
time_running = []

# Set iteration amount and sleep time between changes
iterations = 5
sleep_time = 5

# Base Code
udf_code1 = 'package reconf; \
import org.apache.flink.api.common.functions.FlatMapFunction; \
import org.apache.flink.util.Collector; \
\
import java.io.Serializable; \
\
public class StandardFilterFunction<Long> implements FlatMapFunction<Long, Long>, Serializable { \
\
\
    public StandardFilterFunction() { \
    } \
\
\
    @Override \
    public void flatMap(Long element, Collector<Long> collector) throws Exception { \
\
        boolean result = false; \
\
        if (Integer.parseInt(element.toString()) <='

random_int = 10
udf_code2 = ') { \
            collector.collect(element); \
        } \
    } \
\
}'

# Start Cluster
subprocess.run('./bin/start-cluster.sh')

# Start Query and wait until the command returns
result = subprocess.run('./bin/flink run --detached ../tests/test_timetoapply_standard/standard/target/standard-0.1.jar', stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
job_id = result[result.find("JobID")+6:-1]

time.sleep(sleep_time)

# Loop makes changes to operator file
for i in range(iterations):
    # Change File
    # Determine new random int
    random_int = random.randint(0, 20)
    
    # Replace String in Operator File
    file = open("../tests/test_timetoapply_standard/standard/src/main/java/reconf/StandardFilterFunction.java","w")
    newUDF = udf_code1 + str(random_int) + udf_code2
    file.write(newUDF)
    file.close()

    # Package project
    #subprocess.run('mvn -f ../../quickstart-standard/quickstart package')
    
    # Save Current Time
    time_pre_savepoint.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])

    # Start Query and wait until the command returns
    result = subprocess.run('./bin/flink stop --type native --savepointPath /tmp/flink-savepoints ' + job_id, stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout
    
    savepoint_id = result[result.find("flink-savepoints")+17:-1]

    time_post_savepoint.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])

    result = subprocess.run('./bin/flink run --detached --fromSavepoint /tmp/flink-savepoints/' + savepoint_id + ' ../tests/test_timetoapply_standard/standard/target/standard-0.1.jar', stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stdout

    time_running.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])

    job_id = result[result.find("JobID")+6:-1]
    
    # Wait before making the next change
    time.sleep(sleep_time)


time.sleep(sleep_time)
# Stop Cluster
subprocess.run('./bin/stop-cluster.sh')


# Make certain assertions to check if results are valid
assert len(time_pre_savepoint) == iterations, "Unexpected amount of elements in list: " + str(len(time_pre_savepoint))
assert len(time_post_savepoint) == iterations, "Unexpected amount of elements in list: " + str(len(time_post_savepoint))
assert len(time_running) == iterations, "Unexpected amount of elements in list: " + str(len(time_running))
        

# Write Collected Dates to Output File by appending
# Ignore 1st entry in all but the first list because these entries are created when the job
# is initially started and are thus not part of this test

file = open("./tests/results/timetoapply_standard-results.txt","a")
for i in range(iterations):
    file.write("1: " + time_pre_savepoint[i] + "\n")
    file.write("2: " + time_post_savepoint[i] + "\n")
    file.write("3: " + time_running[i] + "\n")
    file.write("-----------------------------\n")
file.close()


# Remove previous log files
subprocess.run('rm ./log/*', shell=True)

