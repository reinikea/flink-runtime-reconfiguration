package reconf;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import java.io.Serializable;

public class StandardFilterFunction<Long> implements FlatMapFunction<Long, Long>, Serializable {
    protected long count = 0;
    public StandardFilterFunction() {
        count = 0;
    }

    @Override
    public void flatMap(Long element, Collector<Long> collector) throws Exception {

        if (Integer.parseInt(element.toString()) >= 10) {
            collector.collect(element);
        }
    }
}