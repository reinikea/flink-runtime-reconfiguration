package reconf;

import org.apache.commons.io.FileUtils;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.streaming.api.functions.co.CoFlatMapFunction;
import org.apache.flink.streaming.api.functions.co.RichCoFlatMapFunction;
import org.apache.flink.util.Collector;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CompilingFilterFunction<T> extends BroadcastProcessFunction<T, String, T>{

    public static Method thisMethod;
    public static Object instance;

    protected final String name;
    protected final Class valueType;
    protected File sourceFile;

    protected int version;


    public CompilingFilterFunction(String FilterName, Class type) throws Exception {
        name = FilterName;
        valueType = type;
        version = 0;
        
        System.out.println(getCurrentLocalDateTimeStamp());
    }

    public CompilingFilterFunction(String FilterName, Class type, int initialVersion) throws Exception {
        name = FilterName;
        valueType = type;
        version = initialVersion;
        compileSource();

        System.out.println(getCurrentLocalDateTimeStamp());
    }

    // Data Stream
    @Override
    public void processElement(T element, ReadOnlyContext ctx, Collector<T> collector) throws Exception {

        boolean result;

        // Invoke Method
        try {
            result = (boolean) thisMethod.invoke(instance, element);
        } catch (Exception e) {
            result = false;
        }

        if (result) {
            collector.collect(element);
        }
    }

    // Control StreamLong
    @Override
    public void processBroadcastElement(String source, Context ctx, Collector<T> collector) throws Exception {version += 1;
        System.out.println("FO_UDF_Received: " + getCurrentLocalDateTimeStamp());

        version += 1;
        writeFile(source);
        compileSource();
    }

    // Writes and compiles the source file upon receiving changes
    protected void compileSource() throws Exception {

        File root = new File("/java");
        sourceFile = new File("./filters/" + name + "Changes/" + name + Integer.toString(version) + ".java");

        // Compile the source file
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        compiler.run(null, null, null, sourceFile.getPath());

        // Load and instantiate compiled class
        URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {root.toURI().toURL()});
        Class<?> cls = Class.forName("filters." + name + "Changes." + name + Integer.toString(version), true, classLoader);
        instance = cls.getDeclaredConstructor().newInstance();

        Class<?> params[] = {valueType};
        thisMethod = cls.getDeclaredMethod("filter", params);
        System.out.println("FO_UDF_Compiled: " + getCurrentLocalDateTimeStamp());
    }

    // Write new source to source file
    protected void writeFile(String content) throws IOException {
        content = "package filters." + name + "Changes; public class " + name + Integer.toString(version) + " {" + content + "}";

        File newFile = new File("filters/" + name + "Changes/" + name + Integer.toString(version) + ".java");
        FileUtils.write(newFile, content, "UTF-8");
    }


    public String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    }

}


