package reconf;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StandardFilterFunction<Long> implements FlatMapFunction<Long, String>, Serializable {

    public StandardFilterFunction() {
    }

    @Override
    public void flatMap(Long element, Collector<String> collector) throws Exception {

        boolean result = false;

        if (Integer.parseInt(element.toString()) >= 0) {
            collector.collect(getCurrentLocalDateTimeStamp() + "---" + element.toString());
        }
    }

    public String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    }

}
