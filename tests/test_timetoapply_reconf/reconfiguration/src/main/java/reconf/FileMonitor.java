package reconf;

import org.apache.commons.io.FileUtils;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileMonitor extends RichParallelSourceFunction<String> {

    private volatile boolean cancelled = false;
    private Long lastModified;
    protected File sourceFile;
    protected String path;


    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
    }

    public FileMonitor(String path) {
        sourceFile = new File(path);
    }

    @Override
    public void run(SourceContext<String> ctx) throws Exception {
        while(!cancelled) {

            // Source File has changed
            if(lastModified == null || lastModified != sourceFile.lastModified()) {
                System.out.println("FM_Change_Detected: " + getCurrentLocalDateTimeStamp());

                // Read updated file content and store in string
                String content = FileUtils.readFileToString(sourceFile, "UTF-8");

                // Broadcast String
                ctx.collect(content);

                // Last Modified value updated
                lastModified = sourceFile.lastModified();
            }

            // Check for update occurs roughly every second
            Thread.sleep(100);
        }
    }

    @Override
    public void cancel() {
        cancelled = true;
    }


    public String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    }
}
